import './App.css';
import React, {Component} from 'react';
import history from './components/History';
import Routers from './components/Router';
import Footer from './components/Footer';
import Header from './components/Header';
import './App.css';
import './index.js';
import { Router, Link, Route, Switch } from 'react-router-dom';

class App extends Component {
  constructor(){
    super()
  }
  render(){
  return (
      <Router history={history}>         
        <Header/>
        <Routers/>
        <Footer/>
      </Router>                
    );
  }
}
export default App;
