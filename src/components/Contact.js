import React, { Component } from "react";
import createHistory from "history/createBrowserHistory";
import MapContainer from "./MapContainer";
import ConfirmationModal from "./ConfirmationModal";
import "./ConfirmationModal.css";
import { post } from "./ApiCalls";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.successMessageTimeoutHandle = 0;
    this.submitForm = this.submitForm.bind(this);
    this.postedjob = this.postedjob.bind(this);
    this.state = {
      name: "",
      email: "",
      content: "",
      show: false
    };
  }
  showModal = () => {
    this.setState({ show: true });
  };
  hideModal = () => {
    this.setState({ show: false });
  };
  submitForm = evt => {
    evt.preventDefault();
    const form = {
      name: this.state.name,
      email: this.state.email,
      content: this.state.content
    };
    this.postedjob(form);
    this.showModal();
    this.setSuccessMessage("Thank you for your message");
  };
  postedjob = payload => {
    post(
      `https://react-api-arjun.herokuapp.com/api/contact/`,
      payload
    ).then(response => response.json());
  };
  setSuccessMessage(message) {
    clearTimeout(this.successMessageTimeoutHandle);
    this.successMessageTimeoutHandle = setTimeout(() => {
      this.setState({
        successMessage: "",
        name: "",
        email: "",
        content: ""
      });
    });
  }
  render() {
    return (
      <div>
        <main className="main-content">
          <div className="container">
            <div className="page">
              <div className="breadcrumbs">
                <a href="/">Home</a>
                <span>Contact</span>
              </div>
              <div className="content">
                <div className="row">
                  <div className="col-md-4">
                    <h2>Contact</h2>
                    <ul className="contact-detail">
                      <li>
                        <img src="/images/icon-contact-map.png" alt="#"></img>
                        <address>
                          <span>
                            Great Arjun. INC 550 Avenue Street, New york
                          </span>
                        </address>
                      </li>
                      <li>
                        <img src="/images/icon-contact-phone.png" alt=""></img>
                        <a href="tel:1590912831">+1 590 912 831</a>
                      </li>
                      <li>
                        <img
                          src="/images/icon-contact-message.png"
                          alt=""
                        ></img>
                        <a href="mailto:Great@Arjun.com">Great@Arjun.com</a>
                      </li>
                    </ul>
                    <div className="contact-form">
                      <h1>{this.state.currenttask}</h1>
                      <form onSubmit={this.submitForm}>
                        <input
                          type="text"
                          placeholder="name..."
                          value={this.state.name}
                          onChange={e =>
                            this.setState({ name: e.target.value })
                          }
                        />
                        <input
                          type="email"
                          required
                          placeholder="email..."
                          value={this.state.email}
                          onChange={e =>
                            this.setState({ email: e.target.value })
                          }
                        />
                        <input
                          type="text"
                          placeholder="message..."
                          value={this.state.content}
                          onChange={e =>
                            this.setState({ content: e.target.value })
                          }
                        />
                        <button type="submit">Submit</button>
                      </form>
                    </div>
                    <ConfirmationModal
                      show={this.state.show}
                      handleClose={this.hideModal}
                    >
                      <h2>Thank you for your message</h2>
                    </ConfirmationModal>
                  </div>
                  <div className="col-md-7 col-md-offset-1">
                    <div className="map">
                      <MapContainer />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <script
          async
          defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDloXIlH9fsyyeNLHu4nuIZQyJIUviXIhU&callback=initMap"
          type="text/javascript"
        ></script>
      </div>
    );
  }
}
export default Contact;
