// import React, {Component} from 'react';

export const list = api => {
  return fetch(api);
};
export const post = (api, data) => {
  return fetch(api, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  });
};
// export default list;
