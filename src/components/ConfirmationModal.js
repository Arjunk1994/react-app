import React  from 'react';
import './ConfirmationModal.css';
const ConfirmationModal = ({ handleClose, show, children }) => {  
    return (
      <div className={show ? "modal display-block" : "modal display-none"}>
        <section className="modal-main">
          <div className="center">
            <br></br>
            {children}
            <br></br>
            <p><button className="close"  onClick={handleClose}>Close</button></p>
          </div>
        </section>     
      </div>
    );
  };
export default ConfirmationModal