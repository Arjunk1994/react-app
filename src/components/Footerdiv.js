import React from "react"

function FooterDiv(props) {
    return(
        <div className="col-md-2">
            <div className="widget">
                <h3 className="widget-title">{props.name}</h3>
                <ul className="no-bullet">
                <li><a href="#">Lorem ipsum dolor</a></li>
                <li><a href="#">Sit amet consecture</a></li>
                <li><a href="#">Dolorem respequem</a></li>
                <li><a href="#">Invenore veritae</a></li>
                </ul>
            </div>
        </div>
    )
}
export default FooterDiv;