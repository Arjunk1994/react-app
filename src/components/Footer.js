import React from "react"  
import FooterDiv from "./Footerdiv" 
function Footer() {
    return(
        <footer className="site-footer">
            <div className="container">
                <div className="row">
                    <div className="col-md-2">
                        <div className="widget">
                            <h3 className="widget-title">About Us</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia tempore vitae mollitia nesciunt saepe cupiditate</p>
                        </div>
                    </div>      
                <FooterDiv key={1} name='Recent Review'/>
                <FooterDiv key={3} name='Help Center'/>
                <FooterDiv key={2} name='Join Us'/>            
                <div className="col-md-2">
                    <div className="widget">
                        <h3 className="widget-title">Social Media</h3>
                        <ul className="no-bullet">
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Google+</a></li>
                        <li><a href="#">Pinterest</a></li>
                        </ul>
                    </div>
                </div>            
            </div>
        <div className="colophon">Copyright 2014 Company name, Designed by Themezy. All rights reserved</div>
        </div> 
    </footer>
    )        
}
export default Footer