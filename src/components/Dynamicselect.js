import React, {Component} from 'react';

class DynamicSelect extends Component{
    constructor(props){
        super(props)    
    }
    handleChange = (event) =>
    {
        let selectedValue = event.target.value;
        this.props.onSelectChange(selectedValue);
    }
    render(){
        let arrayOfData = this.props.arrayOfData;
        let options = arrayOfData.map((data,index) =>
                <option 
                    key={data.id}
                    value={data.value}>
                    {data.value}
                </option>
            );        
        return (
        <select name="customSearch" className="custom-search-select" onChange={this.handleChange}>
            <option>Select Item</option>
            {options}
        </select>
        )
    }
}
export default DynamicSelect;