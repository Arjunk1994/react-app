import React, {Component} from 'react';
import { Router, Link, Route, Switch } from 'react-router-dom';
import Home from './Home';
import history from './History';
import Contact from './Contact';
import Movie from './Movies';
import Review from './Review';


class Routers extends Component {
    constructor(){
      super()
    }  
    render(){
        return (
            <div>
                <Router history={history}>  
                <Switch>             
                <Route exact={true} path="/" component={Home}/>
                <Route exact={true} path="/contact" component={Contact}/>
                <Route exact={true} path="/movie" component={Movie}/>
                <Route exact path="/review/:id" component={Review} />​
                </Switch> 
                </Router>
            </div>  
            )
        }
    }
export default Routers