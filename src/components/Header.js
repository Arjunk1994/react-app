import React from "react"
import { Router, Link, Route, Switch } from 'react-router-dom';


function Header(props) {
    return(
        <div id="site-content">
            <header className="site-header">
                <div className="container">
                    <a href="/" id="branding">
                    <img src="/images/logo.png" alt="" className="logo"></img>
                    <div className="logo-copy">
                        <h1 className="site-title">Garfield Movie Review</h1>
                        <small className="site-description">Tagline goes here</small>
                    </div>
                    </a>   
                    <div className="main-navigation">
                        <button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>
                        <ul className="menu">
                            <li className="menu-item"><Link to="/">Home</Link></li>
                            <li className="menu-item">About</li> 
                            <li className="menu-item"><Link to="/movie">Movie Review</Link></li>
                            <li className="menu-item">Join us</li>
                            <li className="menu-item"><Link to="/contact">Contact Us</Link></li>               
                        </ul>    
                    </div>     
                    <div className="mobile-navigation"></div>
                </div>
            </header>   
        </div>
    )
}
export default Header;