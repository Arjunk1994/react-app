import React, { Component } from "react";
import { Router, Link, Route, Switch } from "react-router-dom";
import StarRatings from "react-star-ratings";
import { list } from "./ApiCalls";
import DynamicSelect from "./Dynamicselect";
const arrayOfYear = [
  {
    id: "1",
    value: "2018"
  },
  {
    id: "2",
    value: "2019"
  },
  {
    id: "3",
    value: "2020"
  },
  {
    id: "4",
    value: "2021"
  },
  {
    id: "5",
    value: "2022"
  },
  {
    id: "6",
    value: "2023"
  },
  {
    id: "7",
    value: "2024"
  }
];
const arrayOfgenre = [
  {
    id: "1",
    value: "action"
  },
  {
    id: "2",
    value: "drama"
  },
  {
    id: "3",
    value: "fantasy"
  },
  {
    id: "4",
    value: "horror"
  },
  {
    id: "5",
    value: "adventure"
  }
];

class Movies extends Component {
  constructor(props) {
    super(props);
    this.handlenextpage = this.handlenextpage.bind(this);
    this.handlepreviouspage = this.handlepreviouspage.bind(this);
    this.handlesearch = this.handlesearch.bind(this);
    this.handleYear = this.handleYear.bind(this);
    this.handlegenere = this.handlegenere.bind(this);
    this.filtersearch = this.filtersearch.bind(this);
    this.statechangecommon = this.statechangecommon.bind(this);
    this.state = {
      movies: [],
      count: null,
      pages: null,
      nextpage: "",
      previouspage: "",
      searchterm: " ",
      year: "",
      category: "",
      pagenumber: 1
    };
  }
  statechangecommon = (newstate, pagenumber) => {
    this.setState({
      movies: newstate.results,
      count: newstate.count,
      nextpage: newstate.next,
      previouspage: newstate.previous,
      pagenumber: pagenumber
    });
  };
  componentDidMount() {
    list("https://react-api-arjun.herokuapp.com/api/Movie/").then(res => {
      return res.json().then(test => {
        this.statechangecommon(test, 1);
      });
    });
  }
  handleYear = selectedValue => {
    this.setState(
      {
        year: selectedValue
      },
      () => {
        this.filtersearch();
      }
    );
  };
  handlegenere = selectedValue => {
    this.setState(
      {
        category: selectedValue
      },
      () => {
        this.filtersearch();
      }
    );
  };
  filtersearch = () => {
    let year_term = this.state.year;
    let category_term = this.state.category;
    let url_search_filter =
      "https://react-api-arjun.herokuapp.com/api/Movie/?dateofrelease__year=" +
      year_term +
      "&genres=" +
      category_term;
    list(url_search_filter).then(res => {
      return res.json().then(test => {
        this.statechangecommon(test, 1);
      });
    });
    window.history.pushState(
      null,
      null,
      "?year=" + year_term + "&&genres=" + category_term
    );
  };
  handlenextpage = () => {
    list(this.state.nextpage).then(res => {
      return res.json().then(test => {
        this.statechangecommon(test, this.state.pagenumber + 1);
      });
    });
    window.history.pushState(
      null,
      null,
      "?page=" + (this.state.pagenumber + 1)
    );
  };
  handlepreviouspage = () => {
    list(this.state.previouspage).then(res => {
      return res.json().then(test => {
        this.statechangecommon(test, this.state.pagenumber - 1);
      });
    });
    window.history.pushState(
      null,
      null,
      "?page=" + (this.state.pagenumber - 1)
    );
  };
  handlesearch = () => {
    let url_api = this.state.searchterm;
    list(
      "https://react-api-arjun.herokuapp.com/api/Movie/?search=" + url_api
    ).then(res => {
      return res.json().then(test => {
        this.statechangecommon(test, 1);
      });
    });
  };
  render() {
    const { movies } = this.state;
    let buttonnext = null;
    let buttonprev = null;
    if (this.state.previouspage) {
      buttonprev = (
        <a
          href="#"
          onClick={this.handlepreviouspage}
          className="page-number prev"
        >
          <i className="fa fa-angle-left">Prev </i>
        </a>
      );
    }
    if (this.state.nextpage) {
      buttonnext = (
        <a href="#" onClick={this.handlenextpage} className="page-number next">
          <i className="fa fa-angle-right">Next </i>
        </a>
      );
    }
    return (
      <div>
        <main className="main-content">
          <div className="container">
            <div className="page">
              <div className="breadcrumbs">
                <a href="/">Home</a>
                <span>Movie Review</span>
              </div>
              <div className="filters">
                <DynamicSelect
                  arrayOfData={arrayOfYear}
                  onSelectChange={this.handleYear}
                />
                <DynamicSelect
                  arrayOfData={arrayOfgenre}
                  onSelectChange={this.handlegenere}
                />
                <div style={{ float: "Right" }}>
                  <form
                    action="#"
                    className="search-form"
                    onSubmit={this.handlesearch}
                  >
                    <input
                      type="text"
                      required
                      placeholder="Search..."
                      value={this.state.searchterm}
                      onChange={e =>
                        this.setState({ searchterm: e.target.value })
                      }
                    />
                    <button type="submit">
                      <i className="fa fa-search"> search</i>
                    </button>
                  </form>
                </div>
              </div>
            </div>
            <div className="movie-list">
              <div>
                {movies.length > 0 &&
                  movies.map((item, index) => {
                    return (
                      <div>
                        <link
                          href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|"
                          rel="stylesheet"
                          type="text/css"
                        ></link>
                        <link
                          href="/src/dummy/fonts/font-awesome.min.css"
                          rel="stylesheet"
                          type="text/css"
                        ></link>
                        <link rel="stylesheet" href="/src/index.css"></link>
                        <div className="movie">
                          <div key={index}>
                            <div className="col-sm-6 col-md-12">
                              <figure className="movie-poster">
                                <img
                                  src={item.image}
                                  style={{ height: "200px" }}
                                  alt="#"
                                ></img>
                              </figure>
                            </div>
                            <div className="movie-title">
                              <Link to={`/review/${item.id}`}>
                                {item.title}
                              </Link>
                            </div>
                            <h3>
                              Rating :
                              <StarRatings
                                rating={item.rating}
                                starDimension="20px"
                                starSpacing="2px"
                                starRatedColor="yellow"
                                changeRating={this.changeRating}
                                numberOfStars={5}
                                name="rating"
                              />
                            </h3>
                            <button>
                              <Link to={`/review/${item.id}`}>Review</Link>
                            </button>
                            <p>{item.description}</p>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                <div>
                  <div className="pagination">
                    {buttonprev}
                    {buttonnext}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}
export default Movies;
