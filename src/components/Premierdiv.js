import React  from 'react';
const Premierdiv = ({ title,children }) => {  
    return (
        <div className="col-md-4">
        <h2 className="section-title">{title} premiere</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
        <ul className="movie-schedule">
          {children}                            
        </ul> 
      </div>
    );
  };
export default Premierdiv