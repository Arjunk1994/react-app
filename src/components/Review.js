import React, {Component}  from 'react';
import StarRatings from 'react-star-ratings';
class Review extends Component {
    constructor(props){
        super(props);
        this.state={
            review: null,
            movie: null,
            movies:[],            
        }
    }
    componentDidMount() {
        const { params: { id } } = this.props.match; 
        fetch(`https://react-api-arjun.herokuapp.com/api/Movie/${id}/`).then((res) => {   
          res.json().then(response => {
            this.setState({ movies: response , movie:id})            
          })  
        }).catch(err => {
          console.log('err', err)
        })
        
      } 
   render() {  
  return (
        <div>   
            <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css"></link>
            <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css"></link>
            <link rel="stylesheet" href="src/index.css"></link>
            <script src="/static/js/ie-support/html5.js"></script>
            <script src="/static/js/ie-support/respond.js"></script> 
            <main className="main-content">
                <div className="container">
                    <div className="page">
                        <div className="breadcrumbs">
                            <a href="/">Home</a>
                            <span>Movie Review</span>
                        </div>              
                        <div className="content">
                            <div className="row">
                                <div className="col-md-6">
                                    <figure className="movie-poster">
                                    <img height='100%' width='100%' 
                                    src={this.state.movies.image} alt="#">
                                    </img>
                                    </figure>
                                </div>
                                <div className="col-md-6">
                                    <h2 className="movie-title">{this.state.movies.name}</h2>
                                    <div className="movie-summary">
                                        {this.state.movies.synopsis}
                                    </div>
                                    <ul className="movie-meta">
                                        <li><strong>Rating:</strong> 
                                        <StarRatings rating={this.state.movies.rating}
                                         starRatedColor="yellow"  numberOfStars={5} name='rating'/> 
                                        </li>
                                        <li><strong>Length:</strong> 
                                        {this.state.movies.length}</li>
                                        <li><strong>Premiere:</strong> 
                                        {this.state.movies.dateofrelease}</li>
                                        <li><strong>Category:</strong>
                                         {this.state.movies.genres}</li>
                                    </ul>    
                                    <ul className="starring">
                                        <li><strong>Directors:</strong> 
                                        {this.state.movies.director}</li>
                                        <li><strong>Writers:</strong>
                                         {this.state.movies.writer}</li>
                                        <li><strong>Stars:</strong> 
                                        {this.state.movies.staring}</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="entry-content">
                                <p> {this.state.movies.writers_review}</p>
                            </div>
                        </div>
                    </div>                
                </div>
            </main>
        </div>
        )
        
    }
  
}
export default Review;