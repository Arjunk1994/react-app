import React from "react"

function Imagedisplay(props) {
  return(
      <div className={props.classname}>
        <div className="latest-movie">
          <a href={props.item.image}><img  style={{height: props.height}} src={props.item.image} alt="Movie 6" ></img></a>
        </div>
    </div>
  )
}
export default Imagedisplay; 