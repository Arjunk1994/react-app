import React, {Component} from 'react';
import Imagedisplay from "./Imagedisplay"
import PremierMonth from "./PremierMonth"
import Premierdiv from "./Premierdiv"
import SimpleImageSlider from "react-simple-image-slider";


class Home extends Component {
  constructor(props) {
      super(props)      
      this.state = {
        movies : [],
        count:0,
        images:[],
        sideimages:[],
        currentIndex: 0,
        translateValue: 0,
        useGPURender: false,
        showNavs: true,
        showBullets: true,
        navStyle: 2,
        slideDuration: 1,
        bgColor: "#000000",
        slideIndexText: "",
      }
    }
    goToPrevSlide = () => {
      this.setState(prevState => ({
        currentIndex: prevState.currentIndex - 1
      }));      
    }   
    goToNextSlide = () => {
      this.setState(prevState => ({
        currentIndex: prevState.currentIndex + 1
      }));
    }
    componentDidMount() {
      fetch('https://react-api-arjun.herokuapp.com/api/Movie/').then((res) => {
        res.json().then(response => {
          console.log('items', response.count)
          this.setState({ movies: response.results,count: response.count,})
          
        })

      }).catch(err => {
        console.log('err', err)
      })
    }     
    render() {              
        const sliderimages =this.state.movies.map((image,index) => ({url: image.image }));
        const movieimageside = this.state.movies.slice(0,2).map((item,index) =>(
          <Imagedisplay key={item.id} item={item} classname="col-sm-6 col-md-12" height="235px" />));
        const movieimage = this.state.movies.map((item,index) =>(
        <Imagedisplay key={item.id} item={item} classname="col-sm-6 col-md-3" height="200px" />));
        const moviepremierdec = this.state.movies.map((item,index) =>(
          <PremierMonth key={item.id} item={item} month="12" />  
        ));
        const moviepremieroct = this.state.movies.map((item,index) =>(
          <PremierMonth key={item.id} item={item} month="10"/>  
        ));
        const moviepremiernov = this.state.movies.map((item,index) =>(
          <PremierMonth key={item.id} item={item} month="11" />  
        ));      
      return (
    <div>
         <main className="main-content">
          <div className="container">
            <div className="page">
              <div className="row">
                <div className="col-md-9">
                  <div className="slider">
                    <ul style={{position: "absolute", left:" 0px", top: "0px" , width: "98%", height: '500px', overflow: 'hidden'}}  className="slides">
                    <div className="slider " >
                    <SimpleImageSlider
                        width={800}
                        height={504}
                        images={sliderimages}
                        showBullets={this.state.showBullets}
                        showNavs={this.state.showNavs}
                        useGPURender={this.state.useGPURender}
                        navStyle={this.state.navStyle}
                        slideDuration={this.state.slideDuration}
                        onClickNav={this.onClickNav}
                        onClickBullets={this.onClickBullets}
                        onStartSlide={this.onStartSlide}
                        onCompleteSlide={this.onCompleteSlide}
                      />      
                      </div>
                    </ul>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="row">
                    {movieimageside}     
                  </div>
                </div>
              </div> 
              <div className="row">
                {movieimage}
              </div>              
              <div className="row">
                <Premierdiv title='December'>
                    {moviepremierdec}                            
                </Premierdiv>
                <Premierdiv title='November'>
                    {moviepremiernov}                            
                </Premierdiv>
                <Premierdiv title='October'>
                    {moviepremieroct}                            
                </Premierdiv>                
              </div>
            </div>
          </div>
        </main>
        <script src="/static/js/jquery-1.11.1.min.js"></script>
        <script src="/static/js/plugins.js"></script>
        <script src="/static/js/app.js"></script>
      </div>
      )
    }
  }
export default Home;

