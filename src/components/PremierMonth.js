import React from "react"
import { Link } from "react-router-dom"

function PremierMonth(props) {
    if (props.item){
    const dater = new Date(props.item.dateofrelease)
    const start = new Date(props.month+"-1-2019")
    const end = new Date(props.month+"-30-2019")
    if (dater > start && dater < end) {
        return(
            <li>
            <div className="date">{props.month}/19</div>
            <h2 className="entry-title"><Link to={`/review/${props.item.id}`} ><a href="">{props.item.name}</a></Link></h2>
            </li>
        )
      } else {
        return null
      }
    }
}
export default PremierMonth